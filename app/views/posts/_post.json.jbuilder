json.extract! post, :id, :title, :content, :author, :date_published, :comments_count, :created_at, :updated_at
json.url post_url(post, format: :json)
