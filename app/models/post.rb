class Post < ApplicationRecord

  def self.get_reddit_posts
    if @loaded_posts == nil
      Post.load_posts
    else
      @loaded_posts
    end
  end

  def self.load_posts
    url = "https://www.reddit.com/top.json"
    response = nil
    while response == nil || response.to_s.include?('error')
      response = HTTParty.get(url)
    end
    @loaded_posts = Post.fix_data(response.parsed_response)
  end

  def self.fix_data(data)
    data['data']['children'].map{|x| x['data']} if data.has_key?'data'
  end

end
